
<ckage edu.uiuc.cs242.scrabble.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import edu.uiuc.cs242.scrabble.tile.LetterTile;
import edu.uiuc.cs242.scrabble.tile.SpecialTile;

/**
 * A class representing the game itself
 * @author JOJO
 *
 */
public final class Game {
	
	/* The instance of Game */
	private static Game instance;
	
	/* The command of operation */
	private final String PASS = "pass";
	private final String SWAP = "swap";
	private final String PURCHASE = "purchase";
	private final String LETTERTILE = "letter";
	private final String SPECIALTILE = "special";
	private final String END = "end";
	
	/* The number of players in the game */
	private static int numOfPlayer;
	
	/* The iterator of players */
	private final PlayerIter iterator;
	
	/* The board of the game */
	private final Board board;
	
	private Scanner in;
	
	/* The dictionary */
	private Dictionary dictionary;
	
	/**
	 * A function to initiate the instance of game 
	 * @param numOfPlayer the number of players in the game
	 * @return the instance of game
	 */
	public static Game initGame(int numOfPlayer) {
		instance = new Game(numOfPlayer);
		instance.numOfPlayer = numOfPlayer;
		return getCurrentGame();
	}
	
	/**
	 * A function to return current game
	 * @return the game instance
	 */
	public static Game getCurrentGame() {
		return instance;
	}
	
	/**
	 * Constructor
	 * @param num the number of players
	 */
	private Game(int num) {
		
		/* Initialize a list of players */
		List<Player> list = new ArrayList<>();

		for (int i = 1; i <= num; i++) {
			Player player = new Player(i);
			list.add(player);
		}
		
		iterator = new PlayerIter(list); // Instantiate a player iterator
		board = Board.getInstance(); // Get the instance of Board
		dictionary = Dictionary.getInstance(); // Get the instance of dictionary
		
		/* Instantiate the scanner */
		in = new Scanner(System.in);
	}
	
	/**
	 * A function to get the number of players in the game
	 * @return the number of players
	 */
	public static int getNumOfPlayer() {
		return numOfPlayer;
	}
	
	/**
	 * A function to reverse the direction of rounds
	 */
	public void reverseOrder() {
		iterator.reverseOrder();
	}
	
	/**
	 * A function to get the current player in round
	 * @return the current Player
	 */
	public Player currentPlayer() {
		return iterator.currentPlayer();
	}
	
	/**
	 * A function to get the next player in round
	 * @return next player
	 */
	public Player nextPlayer() {
		return iterator.nextPlayer();
	}
	
	/**
	 * A function for player to put the lettertile with the letter in (x, y)
	 * @param player the player
	 * @param letter the letter
	 * @param x coordinate x
	 * @param y coordinate y
	 */
	public void placeLetterTile(Player player, char letter, int x, int y) {
		/* Get the letter tile from player's deck */
		LetterTile tile = player.placeLetterTile(letter);
		
		if (tile != null) { // The player has the letter tile
			
			/* Place the tile on the board */
			if (board.isOccupied(x, y)) {
				player.returnLetterTile(tile);
				System.out.println("The Square has been occupied!");
			} else {
				board.placeTile(tile, x, y);
				System.out.println("The Letter tile " + tile.getLetter() + " has been put at (" + x + ", " + y + ")");
			}
		} else {
			System.out.println("You don't have this letter tile or you have already used it! Please input again!");
		}
	}
	
	
	
	/**
	 * A function to get a list of special tile set by current player
	 * @param x coordinate x
	 * @param y coordinate y
	 * @return a list of special tile
	 */
	public List<SpecialTile> getSpecialTileForPlayer(int x, int y) {
		Player curPlayer = currentPlayer();
		return board.getSpecialTileFromSquare(curPlayer, x, y);
	}
	
	/**
	 * A function to trigger the special tile put on the squares that are placed a letter tile in this round
	 * @param letterTilePlacement the set of letter tile in this round
	 */
	public void triggerSpecialTile(Set<LetterTile> letterTilePlacement) {
		for (LetterTile tile : letterTilePlacement) {
			/* Get the coordinates of letter tile */
			int x = tile.getX();
			int y = tile.getY();
			
			/* All the letter tile removed from the board should be skipped */
			if (x == -1 && y == -1) {
				continue;
			}
			board.triggerSpecialTile(x, y);
		}
	}
	
	
	
	/**
	 * A function to scan the board to get the set of words in this round
	 * @return a set of words
	 */
	
	/**
	 * A function to get the total points the player earns in this round
	 * @param words the list of word
	 * @return the points
	 */
	public int getPointsInTurn(Set<Word> words) {
		int points = 0;
		for (Word word : words) {
			points += word.getPoints();
		}
		return points;
	}
	
	/**
	 * A function to restore the board and player to the initial state in this round
	 * @param player the player
	 * @param letterTilePlacement the set of letter tile placed
	 */
	public void restoreState(Player player, Set<LetterTile> letterTilePlacement) {
		
		/* Remove all the letter tile put in this round */
		board.restoreBoard(letterTilePlacement);
		
		/* Return the placement tile to the player */
		player.restorePlacement();
	}
	
	/**
	 * A function to swap the letter tiles
	 * @param letters the letters
	 * @return the set of new letter tiles by swapping the old tiles 
	 */
	public Set<LetterTile> swap(String[] letters) {
		Player curPlayer = currentPlayer();
		return curPlayer.swapLetterTile(letters);
	}
	
	/**
	 * A function for challenger to challenge a word on the board in this round
	 * @param challenger the challenger player
	 * @param words the set of the challenge word
	 * @return true the challenge success, the current player skip
	 * 		   false the challenge fail, the challenger skip next round
	 */
	public boolean challenge(Player challenger, Set<Word> words) {
		boolean res = false;
		for (Word word : words) {
			/* Get the word in string format */
			String wordStr = word.getWord();
			
			/* Check the word is legal or not */
			boolean isLegal = dictionary.checkWord(wordStr);
			
			/* The word is illegal */
			if (!isLegal) {
				res = true;
				break;
			}
		}
		
		/* Challenge fails, the challenger skip */
		if (!res) {
			challenger.skipRound();
		}
		
		return res;
	}
	
	/**
	 * A function to end current round
	 */
	public void endRound() {
		Player curPlayer = iterator.currentPlayer();
		curPlayer.endRound();
		
		iterator.next();
		
		board.clearLog();
	}
	
	/**
	 * A function to start next round.
	 * The difference betweeen the nextRound and endRound is that, the nextRound() 
	 * only change the status of game, rather than board, and players. This function is often
	 * used to iterator all the players rather than control the process of the game.
	 */
	public void nextRound() {
		iterator.next();
	}
	/**
	 * A function to verify whether the game is over by checking the tilebag
	 * @return true the game is over
	 * 		   false the game can continue
	 */
	public boolean isOver() {
		TileBag bag = TileBag.getInstance();
		return bag.isEmpty();
	}
	
	/**
	 * A function to get the winner of the game
	 * @return the winner
	 */
	public Player getWinner() {
		/* The game over! Find out the winner */
		Player current = iterator.currentPlayer();
		int maxPoints = 0;
		Player winner = null;
		Player tmp = current;
		
		/* Traverse all the players, to find out the player with the highest points */
		do {
			if (current.getPoints() > maxPoints) {
				maxPoints = current.getPoints();
				winner = current;
			}
			current = iterator.next();
		} while (tmp != current);
		
		return winner;
	}
	
	/**
	 * A internal class to iterate the players 
	 * @author lieyongzou
	 *
	 */
	private class PlayerIter implements Iterator<Player> {
		
		/* List of players joining the game */
		private final List<Player> list;
		
		/* The direction of the round(clockwise, counterclockwise) */
		private boolean clockwise;
		
		/* The current index of players in round */
		private int currentIndex;
		
		PlayerIter(List<Player> list) {
			this.list = list;
			
			/* The default direction is clockwise */
			clockwise = true;
			
			currentIndex = 0;
		}

		/**
		 * A function to reverse the direction of rounds
		 */
		public void reverseOrder() {
			clockwise = !clockwise;
		}
		
		@Override
		public boolean hasNext() {
			return !list.isEmpty();
		}
		

		@Override
		public Player next() {
			
			/* Avoid indexOutOfBound Exception */
			if (currentIndex == 0) {
				currentIndex = list.size();
			}
			currentIndex = currentIndex + (clockwise ? 1 : -1); 
			int index = (currentIndex + list.size()) % list.size();
			return list.get(index); // Defensive copy
		}
		
		/**
		 * A function to return the current player
		 * (Multiple call will not change the state of the iterator)
		 * @return the current player
		 */
		public Player currentPlayer() {
			int index = (currentIndex + list.size()) % list.size();
			return list.get(index); // Defensive copy
		}
		
		/**
		 * A function to return the next player
		 * (Multiple call will not change the state of the iterator)
		 * @return
		 */
		public Player nextPlayer() {
			int nextIndex = currentIndex;
			
			/* Avoid the index is negative */
			if (currentIndex == 0) {
				nextIndex = list.size();
			}
			nextIndex = nextIndex + (clockwise ? 1 : -1); 
			int index = (nextIndex + list.size()) % list.size();
			return list.get(index); // Defensive copy
		}
	}
	
}
	/*
	 *  The Following Function are written to control the whole processes via console.
	 *  So the following function will not be tested by JUnit test.
	 *  
	 */
	
	
//	/**
//	 * The process in each round
//	 * (1) Get letter tile from bag
//	 * (2) Place letter tile/Pass/Swap
//	 * (3) Check adjacent
//	 * (4) Scan words
//	 * (5) Ask for challenge
//	 * (6) Count points
//	 * @param player the player in this round
//	 */
//	public void round(Player player) {
//		System.out.println("----------------" + player.toString() + "----------------");
//		
//		/* Verify whether the player is skipped */
//		if (player.isSkipped()) {
//			player.pass();
//			return;
//		}
//		
//		/* Player get the letter tile from bag */
//		player.getTileFromBag();
//		
//		/* Print the board */
//		board.print();
//		 
//		/* Get the original number of letter tiles on the board.
//		 * If it equals 0, the center square must be occupied at the end of this round */
//		int originTileCount = board.getTileCount();
//		
//		String input = "";
//		/* All the placement of tiles should be adjacent. If not, repeat again! */
//		while (true) {
//			/* Put letter tiles */
//			while (!(input = in.nextLine()).equals(END)) {
//				
//				/* Command to parchase a special tile */
//				if (input.trim().equals(PURCHASE)) {
//					purchase(player);
//					continue;
//				}
//				
//				/* Command to pass */
//				if (input.trim().equals(PASS)) {
//					/* Get the letter tile put in this round */
//					Set<LetterTile> letterTilePlacement = player.getLetterTilePlacement();
//
//					/* Return to the statuc before placing all the tile */
//					restoreState(player, letterTilePlacement);
//					player.pass();
//					return;
//				}
//				
//				/* Command to swap */
//				if (input.trim().equals(SWAP)) {
//					System.out.println("Please input the letter you want to swap:");
//					/* Get the letter tile put in this round */
//					Set<LetterTile> letterTilePlacement = player.getLetterTilePlacement();
//					
//					/* Return to the statuc before placing all the tile */
//					restoreState(player, letterTilePlacement);
//					
//					if (swapInConsole(player)) {
//						return;
//					} else {
//						System.out.println("Sorry, you don't have the letter tile you want to swap!");
//						continue;
//					}
//				}
//				String[] command = input.split(" ");
//				
//				/* Get the type of tile (special/letter) */
//				String tileType = command[0];
//				
//				int x = Integer.parseInt(command[2]);
//				int y = Integer.parseInt(command[3]);
//				
//				/* Command to place a speical tile */
//				if (tileType.equals(SPECIALTILE)) {
//					
//					String name = command[1];
//					
//					/* Place the special tile */
//					placeSpecialTile(player, name, x, y);
//					
//				} else if (tileType.equals(LETTERTILE)) {
//					
//					char letter = command[1].charAt(0);
//					
//					/* Place the letter tile */
//					placeLetterTile(player, letter, x, y);
//				}
//				
//			}
//			
//			/* Get the letter tile put in this round */
//			Set<LetterTile> letterTilePlacement = player.getLetterTilePlacement();
//			
//			/* The star square should be occupied or the bonus has been removed. 
//			 * This happens when the letter tile is removed by the boom*/
//			if (!board.isOccupied(7, 7) && originTileCount == 0) {
//				restoreState(player, letterTilePlacement);
//				System.out.println("You should place a letter tile on the center square!");
//				continue;
//			}
//			
//			
//			
//			/* Check the number of cluster in the board, which should be 1 */
//			if (!board.checkAdjacent(letterTilePlacement)) {
//				/* Return to the statuc before placing all the tile */
//				restoreState(player, letterTilePlacement);
//				System.out.println("Sorry, the tiles are not adjacent!! Please place the tile again!");
//				continue;
//			} else {
//				break;
//			}
//		}
//		
//		/* Get the letter tile put in this round */
//		Set<LetterTile> letterTilePlacement = player.getLetterTilePlacement();
//		
//		/* Get the words in this round */
//		Set<Word> words = board.getWordInTurn(letterTilePlacement);
//		
//		System.out.println("The words in this round:" + words);
//		
//		/* Challenge time!! */
//		boolean isSuc = askChallenge(player, words);
//		
//		if (isSuc) {
//			player.pass();
//			restoreState(player, letterTilePlacement);
//			return;
//		}
//		
//		/* Trigger the speical tile in the board if the square is placed by a letter tile */
//		triggerSpecialTile(letterTilePlacement);
//		
//		/* The total points in this round */
//		int points = getPointsInTurn(words);
//		System.out.println("Total score:" + points);
//		
//		/* Add the points to the players */
//		player.updatePoints(points, true);
//		
//		/* Remove the tile the players place in this round */
//		player.endRound();
//		
//		/* The round is over, clear the log */
//		board.clearLog();
//	}
//	
//	/**
//	 * Ask other players whether to challenge the words in this round
//	 * If the challenger successfully challenge the word, the current player pass the round
//	 * If it fails, the challenger skip his futurn round
//	 * @param player the current player
//	 * @param words the list of words in this round
//	 * @return true challenge successfully and the word are illegal
//	 * 		   false challenge fails and the challenger skip the next 
//	 */
//	public boolean askChallenge(Player player, Set<Word> words) {
//		boolean res = false;
//		Player challenger = iterator.next();
//		
//		/* Ask for challenge one by one */
//		while (challenger != player) {
//			System.out.print(challenger.name() + " Do you want to challenge (yes or no)?");
//			
//			/* Get command from console */
//			String command = in.nextLine();
//			if (command.equals("yes")) {
//				res = challenge(challenger, words);
//				
//			}
//			challenger = iterator.next();
//		}
//		return res;
//	}
//	
//	/**
//	 * A function to purchase a special tile
//	 * @param player the player
//	 */
//	public void purchase(Player player) {
//		/* Print the price and id in the store */
//		for (SpecialTileStore tile : SpecialTileStore.values()) {
//			System.out.println(tile);
//		}
//		/* Start shopping!! */
//		String input = "";
//		while (!(input = in.nextLine()).trim().equals("end")) {
//			String name = input.trim();
//			player.purchaseTile(name);
//		}
//	}
//	
//	/**
//	 * Swap the letter tile
//	 * @param player the current player
//	 * @return true the swap successful
//	 * 		   false the swap operation stop
//	 */
//	public boolean swapInConsole(Player player) {
//		/* Get the letter tile that players want to swap */
//		String input = in.nextLine().trim();
//		String[] letterTileSwap = input.split(" ");
//		while (!(player.swapLetterTile(letterTileSwap)).isEmpty()) {
//			System.out.println("Sorry, you don't have that letter tile!! Please input again!");
//			input = in.nextLine().trim();
//			
//			/* Player change his idea and don't want to swap */
//			if (input.equals(END)) {
//				return false;
//			}
//			letterTileSwap = input.split(" ");
//		} 
//		return true;
//		
//	}
//	
//	/**
//	 * A function to start the game
//	 */
//	public void start() {
//		Player current = iterator.currentPlayer();
//		TileBag bag = TileBag.getInstance();
//		while (!bag.isEmpty()) {
//			round(current);
//			current = iterator.next();
//		}
//		
//		/* The tile bag is empty, the game stop.*/
//		end();
//	}
//	
//	/**
//	 * A function to stop the game and get the winner!!
//	 */
//	public void end() {
//		Player winner = getWinner();
//		System.out.println("Game is Over!! The Winner is " + winner);
//	}
//	
//}
//
//
? phpinfo(); ?>

