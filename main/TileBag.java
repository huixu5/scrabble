
<ckage edu.uiuc.cs242.scrabble.core;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.uiuc.cs242.scrabble.tile.Boom;
import edu.uiuc.cs242.scrabble.tile.Greedy;
import edu.uiuc.cs242.scrabble.tile.LetterTile;
import edu.uiuc.cs242.scrabble.tile.NegativePoint;
import edu.uiuc.cs242.scrabble.tile.ReverseOrder;
import edu.uiuc.cs242.scrabble.tile.Skip;
import edu.uiuc.cs242.scrabble.tile.SpecialTile;

/**
 * A class of tile bay. Each game only have one tile bag, all the palyers
 * share the tile bag. All the tile in bag are randomly fetched. If the bag is empty,
 * the game ends.
 * A bag also serve as a special tile store, which sells Boom, Negative Point, Reverse Order, Skip and Greedy, with their price
 * The type of special tile has BOOM, NegativePoint, ReverseOrder, Shield
 * @author JOJO
 *
 */
public final class TileBag implements Iterable<LetterTile>{
	
	/* The letter with one point */
	private final char[] ONE_POINT = {'A', 'E', 'I', 'L', 'N', 'O', 'R', 'S', 'T', 'U'};
	
	/* The letter with two points */
	private final char[] TWO_POINT = {'D', 'G'};
	
	/* The letter with three points */
	private final char[] THREE_POINT = {'B', 'C', 'M', 'P'};
	
	/* The letter with four points */
	private final char[] FOUR_POINT = {'F', 'H', 'V', 'W', 'Y'};
	
	/* The letter with five points */
	private final char[] FIVE_POINT = {'K'};
	
	/* The letter with eight points */
	private final char[] EIGHT_POINT = {'J', 'X'};
	
	/* The letter with ten points */
	private final char[] TEN_POINT = {'Q', 'Z'};
	
	/* The set of tiles in the bag.
	 * The value of array is the amount of tile with the letter left
	 * For example, if tiles[2] = 3, the letter tile C has three left*/
	private final int[] tiles;
	
	/* The total amount of letter tile left in the bag */
	private int count;
	
	/* A map stores the letter and its corresponding points */
	private final Map<Character, Integer> letterPoints;
	
	/* The instance of tile bag */
	private static TileBag instance = new TileBag();
	
	/* The instance of game */
	private Game game;
	
	
	/**
	 * A function to get the static instance of tile bag
	 * @return a instance of tile bag
	 */
	public static TileBag getInstance() {
		return instance;
	}
	
	private TileBag() {
		
		/* Get the game instance */
		game = Game.getCurrentGame();
		
		/* The number of each letter tile is followed by the rules of English scrabble, which has totally 98 tiles.
		 *  E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4
		 *  D ×4, G ×3
		 *  B ×2, C ×2, M ×2, P ×2
		 *  F ×2, H ×2, V ×2, W ×2, Y ×2
		 *  K ×1
		 *  J ×1, X ×1
		 *  Q ×1, Z ×1  */
		tiles = new int[]{9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2, 1, 6, 4, 6, 4, 2, 2, 1, 2, 1};
		count = 98;
		
		/* Initialize the map, put all the letter and its value into the map */
		letterPoints = generateMapWithPoints();

	}
	
	/**
	 * A function to judge whether the bag is empty
	 * @return true the bag is empty, the game is over
	 * 		   false the bag is not empty, the game can continue
	 */			
	public boolean isEmpty() {
		return count == 0;
	}
	
	/**
	 * A funtion to get the speical tile type by name
	 * @param name the name of special tile
	 * @return the special tile
	 */
	public SpecialTile pickSpcialTile(String name) {
		Player curPlayer = game.currentPlayer(); 
		switch(name) {
		case "BOOM":
			return new Boom(SpecialTileStore.BOOM.getPrice(), curPlayer);
		case "NEGATIVEPOINT":
			return new NegativePoint(SpecialTileStore.NEGATIVEPOINT.getPrice(), curPlayer);
		case "REVERSEORDER":
			return new ReverseOrder(SpecialTileStore.REVERSEORDER.getPrice(), curPlayer);
		case "SKIP":
			return new Skip(SpecialTileStore.SKIP.getPrice(), curPlayer);
		case "GREEDY":
			return new Greedy(SpecialTileStore.GREEDY.getPrice(), curPlayer);
		default:
			return null;
		}
	}
	
	/**
	 * A function to generate a map which records the letter and its corresponding points
	 * @return a map with <letter, points>
	 */
	private Map<Character, Integer> generateMapWithPoints() {
		Map<Character, Integer> res = new HashMap<>();
		/* One point */
		for (char letter : ONE_POINT) { 
			res.put(letter, 1);
		}
		/* Two points */
		for (char letter : TWO_POINT) {
			res.put(letter, 2);
		}
		/* Three points */
		for (char letter : THREE_POINT) {
			res.put(letter, 3);
		}
		/* Four points */
		for (char letter : FOUR_POINT) {
			res.put(letter, 4);
		}
		/* Five points */
		for (char letter : FIVE_POINT) {
			res.put(letter, 5);
		}
		/* Eight points */
		for (char letter : EIGHT_POINT) {
			res.put(letter, 8);
		}
		/* Ten points */
		for (char letter : TEN_POINT) {
			res.put(letter, 10);
		}
		
		return res;
	}
	
	@Override
	public Iterator<LetterTile> iterator() {
		// TODO Auto-generated method stub
		return new LetterTileIter();
	}
	
	
	/**
	 * A enum containing all the special tile type and its price
	 *
	 */
	public enum SpecialTileStore {
		BOOM(1), NEGATIVEPOINT(1), REVERSEORDER(1), SKIP(1), GREEDY(1);
		
		/* The corresponding price of the special tile */
		private final int price;
		
		/**
		 * Constructor
		 * @param price the price of special tile
		 */
		SpecialTileStore(int price) {
			this.price = price;
		}
		
		
		/**
		 * A function to get the price of special tile
		 * @return the price
		 */
		public int getPrice() {
			return price;
		}
		
		/**
		 * A funtion to get the speical tile type by name
		 * @param name the name of special tile
		 * @return the special tile type
		 */
		public static SpecialTileStore pickSpcialTileType(String name) {
			switch(name) {
			case "BOOM":
				return BOOM;
			case "NEGATIVEPOINT":
				return NEGATIVEPOINT;
			case "REVERSEORDER":
				return REVERSEORDER;
			case "SKIP":
				return SKIP;
			case "GREEDY":
				return GREEDY;
			default:
				return null;
			}
		}
	}

	
	/**
	 * A class to iterate the letter tile
	 *
	 */
	private class LetterTileIter implements Iterator<LetterTile> {

		@Override
		public boolean hasNext() {
			return count > 0;
		}

		@Override
		public LetterTile next() {
			
			List<LetterTile> list = new ArrayList<>();
			
			/* Instantiate a random picker*/
			Random r = new Random();
			
			int random = r.nextInt(count);
			
			/* Pick a letter according to the random number */
			int i = 0;
			while (random > tiles[i]) {
				random -= tiles[i];
				i++;
			}
			char letter = (char)('A' + i);
			
			tiles[i]--;
			count--;
			
			/* Instantialte a letter tile */
			return new LetterTile(letter, letterPoints.get(letter));
		}
		
	}
}
? phpinfo(); ?>

