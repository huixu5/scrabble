
<ckage edu.uiuc.cs242.scrabble.core;

import java.util.List;

import edu.uiuc.cs242.scrabble.tile.LetterTile;

/**
 * A class representing the word
 * @author JOJO
 *
 */
public class Word {
	
	/* The word string */
	private String word;
	
	/* The list of tiles forming the word */
	private final List<LetterTile> tiles;
	
	/* The total points of the word */
	private int points;
	
	/* The bonus of word points */
	private int bonus;
	
	/**
	 * Constructor
	 * @param tiles the letter tiles placed in this round
	 */
	public Word(List<LetterTile> tiles) {
		this.tiles = tiles;
		bonus = 1;
		/* Set the initial value of the points */
		points = Integer.MAX_VALUE;
		
		/* Get the the word in string format */
		word = formWord();
	}
	
	/**
	 * A function to get the points of the word
	 * @return the point
	 */
	public int getPoints() {
		if (points == Integer.MAX_VALUE) {
			points = countPoints();
		}
		return points;
	}
	
	/**
	 * A function to get the word
	 * @return the string of word
	 */
	public String getWord() {
		return word;
	}
	
	@Override
	public String toString() {
		return word;
	}
	
	/**
	 * A function to get the total points of the word
	 * A point that needs to be consider is that before counting the points, it should check whether the tile
	 * has been removed by boom
	 * @return the points the points of the left letter tile
	 */
	private int countPoints() {
		
		
		int count = 0;
		for (LetterTile tile : tiles) {
			/* Check whether the tile has been removed by a boom */
			if (tile.getX() == -1 && tile.getY() == -1) {
				continue;
			}
			count += checkBonus(tile);
			
			/* Check the word trigger the negative point tile, 
			 * and the points is negative or not */
			if (tile.isNegative()) {
				bonus = -bonus;
			}
		}
		return count * bonus;
	}
	
	/**
	 * A function to check the bonus of one Square, and get the points of tile with bonus
	 * If the square is DLS or TLS, the points will be doubled or tripled
	 * If the square is DWS or TWS, the whole word's points will be doubled or tripled
	 * @param tile the tile
	 * @return the points of tile with bonus
	 */
	private int checkBonus(LetterTile tile) {
		/* Get the coordinate of the tile */
		int x = tile.getX();
		int y = tile.getY();
		int points = tile.getPoint();
		
		/* Get the instance of Board */
		Board board = Board.getInstance();
		
		/* Get the type of the square */
		Square.SquareType type = board.getBonus(x, y);
		
		/* Process each bonus */
		switch (type) {
		case DLS :
			points *= 2;
			board.removeBonus(x, y);
			break;
		case TLS :
			points *= 3;
			board.removeBonus(x, y);
			break;
		case DWS : 
			bonus *= 2;
			board.removeBonus(x, y);
			break;
		case TWS :
			bonus *= 3;
			board.removeBonus(x, y);
			break;
		default:
			break;
		}
		return points;
	}
	
	/**
	 * A function to catecate the letter into a word
	 * @return a word
	 */
	private String formWord() {
		StringBuilder sb = new StringBuilder();
		for (LetterTile tile : tiles) {
			sb.append(tile.getLetter());
		}
		return sb.toString();
	}
}
? phpinfo(); ?>

