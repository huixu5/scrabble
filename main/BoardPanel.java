
<ckage edu.uiuc.cs.cs242.scrabble.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

import edu.uiuc.cs.cs242.scrabble.core.Board;
import edu.uiuc.cs.cs242.scrabble.core.Square.SquareType;
import edu.uiuc.cs.cs242.scrabble.gui.ScrabbleController.MessageToController;
import edu.uiuc.cs.cs242.scrabble.tile.LetterTile;

public class BoardPanel extends JPanel implements Observer {
	
	/* The commands from the controller */
	private final String SYNC = "sync"; // A command to synchronize the board
	private final String SHOWSPEC = "showSpecial";
	
	private final int ROW_COLUMN_NUMBER = 15;
	
	private final String COMMAND_PATTERN = "%s %s %d %d";
	
	/* The board in the game */
	private Board board;
	
	/* The panel for the board */
	private JPanel boardPanel;
	
	/* The message to the controller */
	private MessageToController message;
	
	public BoardPanel() {
		board = Board.getInstance();
		
		message = ScrabbleController.getInstance().getMessageToController();
		
		/* register to the controller */
		ScrabbleController.getInstance().register(this);
		
		/* The layout should be a grid layout to draw a grid */
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(650, 690));
		setBackground(Color.decode("#ffe6b3"));
		
		/* Design two borders and combine together */
		Border compound = BorderFactory.createCompoundBorder(
				BorderFactory.createRaisedBevelBorder() , BorderFactory.createLoweredBevelBorder());
		setBorder(compound);
		
		/* A panel for the board */
		boardPanel = new JPanel();
		boardPanel.setLayout(new GridBagLayout());
		drawGrid(boardPanel);
	
		add(boardPanel);
		
		/* A panel for the letter tile */
		TilePanel letterPanel = new TilePanel();
		add(letterPanel);
		
	}
	
	/**
	 * A function to draw the grid
	 */
	private void drawGrid(JPanel panel) {
		panel.removeAll();
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		for (int row = 0; row < ROW_COLUMN_NUMBER; row++) {
             for (int col = 0; col < ROW_COLUMN_NUMBER; col++) {
                 gbc.gridx = col;
                 gbc.gridy = row;
                 
                 /* Get the type of the square panel */
                 SquarePanelType type = getSquarePenelType(board.getBonus(row, col));
                 
                 SquarePanel cellPane = new SquarePanel(row, col, type);
                 Border border = null;
                 if (row < ROW_COLUMN_NUMBER - 1) {
                     if (col < ROW_COLUMN_NUMBER - 1) {
                         border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                     } else {
                         border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                     }
                 } else {
                     if (col < ROW_COLUMN_NUMBER - 1) {
                         border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                     } else {
                         border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                     }
                 }
                 cellPane.setBorder(border);
                 
                 /* Add the existed letter tile to the squarePanel */
                 if (board.isOccupied(row, col)) {
                	 LetterTile tile = board.getLetterTileFromSquare(row, col);
                	 JLabel label = new JLabel(getIcon(String.valueOf(tile.getLetter())));
                	 cellPane.add(label);
                	 cellPane.setOccupied(); // Set the squarePanel is occupied, so that other letter tile cannot place on it
                 }
                 panel.add(cellPane, gbc);
             }
         }
		panel.invalidate();
		panel.repaint();
	}
	
	/**
	 * A function to get the icon
	 * 
	 * @param name
	 *            the name of the img
	 * @return the icon
	 */
	private ImageIcon getIcon(String name) {
		String path = "src/main/resources/%s.png";
		ImageIcon icon = null;
		try {
			BufferedImage img = ImageIO.read(new File(String.format(path, name)));
			icon = new ImageIcon(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return icon;
	}
	
	/**
	 * A function to get the square panel type according to the square type
	 * @param type square type
	 * @return the square panel type
	 */
	private SquarePanelType getSquarePenelType(SquareType type) {
		switch(type) {
		case DLS:
			return SquarePanelType.DLS;
		case TLS:
			return SquarePanelType.TLS;
		case DWS:
			return SquarePanelType.DWS;
		case TWS:
			return SquarePanelType.TWS;
		default:
			return SquarePanelType.REGULAR;
		}
	}
	
	public class SquarePanel extends JPanel {
		
		/* The coordinates of square in the board */
		private final int xPos;
		private final int yPos;
		
		private SquarePanelType type;
		
		private JLabel current;
		private JLabel backup; 
		
		/* A boolean showing the current square is occupied or not */
		private boolean occupied = false;
		
		public SquarePanel(int x, int y, SquarePanelType type) {
			
			this.xPos = x;
			this.yPos = y;
			
			setPreferredSize(new Dimension(41, 41));
			((FlowLayout)getLayout()).setVgap(0);
			new PanelDropTargetListener(this);
			
			/* Set the background color of the square */
			this.type = type;
			setBackground(type.getColor());
			
			/* Set the text of the square */
			if (type != SquarePanelType.REGULAR) {
				String content = "<html><font style='font-weight:bold;' color='white'>" + type.name() + "</font></html>";
				JLabel label = new JLabel(content, JLabel.CENTER);
				label.setPreferredSize(new Dimension(40, 40));
				current = label;  // Set the current label
				add(label);
			}
			
			/* Add a click action. 
			 * If player double click the square, the special tiles he placed on this square will be showed up*/
			addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {}
				
				@Override
				public void mousePressed(MouseEvent e) {}
				
				@Override
				public void mouseExited(MouseEvent e) {}
				
				@Override
				public void mouseEntered(MouseEvent e) {}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					/* Player can double click the square to get what he placed on that square  */
					if (e.getClickCount() == 2) {
						message.changeStatus(SHOWSPEC + " " + xPos + " " + yPos);
					}
					
				}
			});
		}
		
		/**
		 * A function to check whether the square has been occupied by a letter tile
		 * @return true the square is occupied
		 * 		   false the square is empty
		 */
		public boolean isOccupied() {
			return occupied;
		}
		
		
		public void setOccupied() {
			occupied = true;
		}
		
		/**
		 * A function to hide current label and show the tile
		 * @param label the letter tile
		 */
		public void placeTile(String name) {
			
			if (occupied) {
				return;
			}
			
			/* The command to the controller */
			String command = "";
			/* Check the type of tile 
			 * The length of letter tile should be 1*/
			if (name.length() ==1) {
				/* Has Bonus */
				if (current != null && current.getText() != null) {
					backup = current; // backup the label
					remove(current);
				}
				
				/* Create a new tile label by the name */
				DragTile tile = new DragTile(name.charAt(0));
				add(tile);
				occupied = true;
				current = tile;
				
				command = String.format(COMMAND_PATTERN, "letter", tile.getName(), xPos, yPos);
				
			} else {
				command = String.format(COMMAND_PATTERN, "special", name, xPos, yPos);
			}
			
			/* Send the message */
			System.out.println(command);
			message.changeStatus(command);			
		}
		
		/**
		 * If the letter tile is removed from current square, the previous bonus should be returned
		 */
		public void restoreLabel() {
			if (current == null || !occupied) {
				return;
			}
			DragTile label = (DragTile)current;
			remove(current);
			current = null;
			if (backup != null) {
				add(backup);
				current = backup;
				backup = null;
			}
			occupied = false;
			
			/* Send the message  */
			String command = String.format(COMMAND_PATTERN, "removeLetter", label.getName(), xPos, yPos);
			message.changeStatus(command);
		}
	}
	
	private enum SquarePanelType {
		DLS(Color.decode("#00ffff")), TLS(Color.decode("#0000ff")), DWS(Color.decode("#ff33cc")), 
		TWS(Color.decode("#ff0000")), REGULAR(Color.decode("#ffe6b3"));
		
		private Color color;
		
		private SquarePanelType(Color color) {
			this.color = color;
		}
		
		/**
		 * A function to get the color of the square
		 * @return the color
		 */
		public Color getColor() {
			return this.color;
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg == null || !(arg instanceof String)) {
			return;
		}
		String command = (String)arg;
		if (command.equals(SYNC)) {
			drawGrid(boardPanel);
		}
		
	}
}
? phpinfo(); ?>

