
<ckage edu.uiuc.cs242.scrabble.core;

import edu.uiuc.cs242.scrabble.tile.LetterTile;
import edu.uiuc.cs242.scrabble.tile.SpecialTile;
import edu.uiuc.cs242.scrabble.tile.Tile;

/**
 * A class representing a square on the board
 * @author JOJO
 *
 */
public class Square {
	/* The coodination of position */
	private final int x, y;
	
	/* The letter tile occupies the position.
	 * If the letterTile is null, the position is not occupied  */
	private LetterTile letterTile;
	private List<SpecialTile> specialList;
	
	/* The type of the position(Regular, DLS, TLS, DWS, TWS) */
	private SquareType type;
	
	/**
	 * Constructor
	 * @param x the coordinate x
	 * @param y the coordinate y
	 * @param type the type of square
	 */
	public Square(int x, int y, SquareType type) {
		this.x = x;
		this.y = y;
		this.type = type;
		specialList = new ArrayList<>();
	}
	
	public boolean setTile(Tile tile) {
	
		/* Set the position of the tile */
		tile.setPosition(x, y);
		if (tile instanceof LetterTile) {
			letterTile = (LetterTile)tile;
		} else if (tile instanceof SpecialTile){
			specialList.add((SpecialTile)tile);
		}
		return true;
	}
	

	/**
	 * A function to remove the letter tile on the position
	 * through set the points to -1,-1(removed from the board)
	 * @return the letter tile 
	 */
	public LetterTile clearTile() {
		LetterTile tile = letterTile;
		
		if (tile != null) {
			tile.setPosition(-1, -1);
		}
		/* Remove the letter tile */
		letterTile = null;
		return tile;
	}
	
	
	public enum SquareType {
		REGULAR, DLS, TLS, DWS, TWS;
	}

	public void boun() {
		if (specialList.isEmpty()) {
			return;
		}
		for (SpecialTile tile : specialList) {
			tile.trigger();	
		}
		specialList.clear();
	}
	
}
? phpinfo(); ?>

